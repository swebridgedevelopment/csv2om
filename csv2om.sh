#!/bin/bash
#===================================================================#
#                       CSV 2 O&M script                        	#
#===================================================================#
# This script converts data from a csv file to O&M files, adding    #
# depth and position                                                #
#===================================================================#
# @author : Enoc Martínez                                           #
# @contact : enoc.martínez@upc.edu                                  #
# @organization : Universitat Politècnica de Catalunya	(UPC)       #
#                                                                   #
# This script has been developed as a part of the NeXOS project     #
#===================================================================#


#-------- Insert Result constants --------#
# This variables are used to create the XML templates
before_template='<?xml version="1.0" encoding="UTF-8"?>
<sos:InsertResult xmlns:sos="http://www.opengis.net/sos/2.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" service="SOS" version="2.0.0"
    xsi:schemaLocation="http://www.opengis.net/sos/2.0 http://schemas.opengis.net/sos/2.0/sos.xsd">
    <sos:template>'

after_template='</sos:template>
    <sos:resultValues>'

ending='    </sos:resultValues>
</sos:InsertResult>'

template=$3 # The template should b the third argument

#attaching the different parts of the header
header=$before_template$template$after_template


#----- Convert to degrees-----#
# This functions takes the position and converts it to
# decimal degrees. If the axis is S or W the number is set
# to negative
# Arguments:
# 	1 : Varible to converted 
convert_to_degrees(){
	# arg1 should be a string
	in=$1
	echo "converting $in to degrees"

	# if the first value is a 0 ignore it
	if [ ${in:0:1} == '0' ]; then
		in=${in:1}
	fi

	deg=${in:0:2} # get the degrees
	rest=${in:2: -1} # ge the rest
	axis=${in: -1} # get axis
	
	echo "degs $deg, $axis, rest: $rest"
	sign='1' # Set positive value by default
	if [ $axis == 'S' ] || [ $axis == 'W' ]; then
		# if the axis is North or South the value is negative
		sign='-1' 

	fi
	# calculate the result an store it in a temporal variable
	tmp_result=$(bc <<< "scale=8; $sign*($deg+($rest/60))")
}

#---------------------------------------------------------------------------#
#--------------------------- Program starts here ---------------------------#
#---------------------------------------------------------------------------#
# Check arguments
if [ $# != 3 ]; then
	echo "usage: $0 <csv file> <position file> <template>"
	exit
fi

csvFile=$1 # csv data file
technicalFile=$2 # file where the latitude and longitude are stored
template=$3 # template identifier for the Insert Result file

echo "Looking for platform position in $technicalFile..."
# getting the positoin info from the technical file, it is expected to be
# at the third and fourth fields of the line 10

# Get the line 10
line10=$(sed -n  -e 10p $technicalFile)
# get the third element, cut from '=' 
latitude_raw=$(echo $line10 | awk '{ print $3 }' |  cut -d '=' -f2 )
# get the fourth element, cut from '='
longitude_raw=$(echo $line10 | awk '{ print $4 }' |  cut -d '=' -f2)
echo "latitude raw $latitude_raw, longitude_raw $longitude_raw"

# Getting latitude and longitude in decimal degrees
convert_to_degrees $latitude_raw 
latitude=$tmp_result
convert_to_degrees $longitude_raw 
longitude=$tmp_result

echo "Latitude: $latitude, longitude: $longitude"

echo ""

# Start decoding the csv file
echo "Start decoding the file $csvFile..."
outputFile=''

# Reading CSV file line by line
while IFS='' read -r line || [[ -n "$line" ]]; do
	# If the firs char is not 1 (not a timestamp in epoch), ignore the whole line
	if [ ${line:0:1} != '1' ]; then
		echo "...ignoring line $line"
	else
		# strip carriage return if any
		line=$(echo $line | sed "s/\r//g")

		#----Converting timestamp-----#
		# from epoch to O&M
		timestamp_raw=$(echo "$line" | cut -d ';' -f1) # get the first field
		day=$(date -d@"$timestamp_raw" -I) # get the date
		time=$(date -d@"$timestamp_raw" | awk '{print $4}') # get the time
		timestamp="${day}T${time}Z" # paste day and time

		depth=$(echo "$line" | cut -d ';' -f2) # get the depth
		# remove the depth, and then remove the duplicated separator by a single separator
		line=$( echo $line | sed "s/$depth//" | sed "s/;;/;/")
		
		# Search and replace the old timestamp by the new, and also the ; for #		
		newLine=$(echo $line | sed "s/$timestamp_raw/$timestamp/" | sed "s/;/#/g" )
		# attach the latitude+longitude+depth and the block separator
		newLine+="#$latitude#$longitude#$depth@"
		
		
		# If the outputFile is not created, generate it
		if [ ${#outputFile} == 0 ]; then
			outputFile="$(echo $csvFile | cut -d '.' -f1)-converted.xml"
			printf  "$header" > $outputFile			
		fi

		# append the newLine
		printf "$newLine" >> $outputFile 		
		
	fi
done < "$csvFile"
printf $ending >> $outputFile # append the file ending
echo "file decoded"
echo "generated file \"$outputFile\""

